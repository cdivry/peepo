#!/usr/bin/env python3

###
# IMPORTS
###

#from os import environ
#environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
import pygame
import json
import datetime
import sys
from pygame.math import Vector3
import time
import os
import base64
import random

###
# FUNCTIONS
###

def get_version():
    with open('peepo.py', 'r') as fd:
        buf = fd.read()
    v_bytes = base64.b64encode(buf.encode("utf-8"))
    v = str(v_bytes, "utf-8").replace("=", "")[::-1][0:42]
    return (v)

###
# ENTITY
###

class Entity():

    def log(self, msg):
        horo = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        line = '[' + horo + ']: ' + msg
        with open('logs.txt', 'a') as fd:
            fd.write(line + '\n')
        print(line)

    def look_var(self, _dict, _key, _default=None):
        if _key not in _dict:
            return (_default)
        return (_dict[_key])

###
# CUBE
###

class Cube(Entity):

    def __init__(self, game, center, grid_pos):
        self._vectors = self.init_vectors(center)
        self._angle = 0.0
        self._screen_width = game.width
        self._screen_height = game.height
        self.grid_pos = grid_pos
        self.type = game.map.tiles[int(grid_pos.y) * game.map.width + int(grid_pos.x)]
        self.color = tuple(game.map.grounds[self.type]["color"])
        self.name = game.map.grounds[self.type]["name"]
        #image = game.map.grounds[self.type]["image"]
        #self.image = None
        #if _img is not None:
        #    self.image = pygame.image.load(_img)
        #    self.image = pygame.transform.scale(self.image, (50,50)) # reduce img quality
        # items
        self.items = []
        _its = [ it for it in game.map.tiles_item ]
        for item in _its:
            _pos = Vector3(item["position"][0], item["position"][1], 0)
            if int(_pos.x) == int(grid_pos.x) and int(_pos.y) == int(grid_pos.y):
                self.items.append(item)
        # decors
        self.decors = []
        _dec = [ d for d in game.map.tiles_decor ]
        for decor in _dec:
            _pos = Vector3(decor["position"][0], decor["position"][1], 0)
            if int(_pos.x) == int(grid_pos.x) and int(_pos.y) == int(grid_pos.y):
                self.decors.append(decor)
        # Define the vectors that compose each of the 6 faces
        self._faces  = [(0,1,2,3),
                       (1,5,6,2),
                       (5,4,7,6),
                       (4,0,3,7),
                       (0,4,5,1),
                       (3,2,6,7)]
        self._faces  = [(5,4,7,6),] # only upper side

    def points(self):
        return (self._vectors)

    def color(self):
        return (self._color)

    def init_vectors(self, center):
        size = 0.5
        return ([
            Vector3(center.x - size, center.y + size, center.z - size),
            Vector3(center.x + size, center.y + size, center.z - size),
            Vector3(center.x + size, center.y - size, center.z - size),
            Vector3(center.x - size, center.y - size, center.z - size),
            Vector3(center.x - size, center.y + size, center.z + size),
            Vector3(center.x + size, center.y + size, center.z + size),
            Vector3(center.x + size, center.y - size, center.z + size),
            Vector3(center.x - size, center.y - size, center.z + size)
        ])

    def transform_vectors(self, vectors, new_angle):
        transformed_vectors = []
        for vector in vectors:
            mod_vector =     vector.rotate_x(new_angle.x)
            mod_vector = mod_vector.rotate_y(new_angle.y)
            mod_vector = mod_vector.rotate_z(new_angle.z)
            mod_vector = self._project(mod_vector, self._screen_width, self._screen_height, 256, 4)
            transformed_vectors.append(mod_vector)
        return (transformed_vectors)

    def _project(self, vector, win_width, win_height, fov, viewer_distance):
        factor = fov / (viewer_distance + vector.z)
        x = 1.0 + vector.x * factor + win_width / 2
        y = -vector.y * factor + win_height / 2
        return (Vector3(x, y, vector.z))

    def calculate_average_z(self, vectors):
        avg_z = []
        for i, face in enumerate(self._faces):
            z = (vectors[face[0]].z +
                 vectors[face[1]].z +
                 vectors[face[2]].z +
                 vectors[face[3]].z) / 4.0
            avg_z.append([i, z])
        return avg_z

    def get_face(self, index):
        return (self._faces[index])

    def create_polygon(self, face, transformed_vectors):
        return ([
            (transformed_vectors[face[0]].x, transformed_vectors[face[0]].y),
            (transformed_vectors[face[1]].x, transformed_vectors[face[1]].y),
            (transformed_vectors[face[2]].x, transformed_vectors[face[2]].y),
            (transformed_vectors[face[3]].x, transformed_vectors[face[3]].y),
            (transformed_vectors[face[0]].x, transformed_vectors[face[0]].y)
        ])


###
# GAME
###

class Game(Entity):

    running = False
    fps = None
    clock = None
    display = None
    map = None
    font = None
    zoom = 1.0
    game_speed = 2.0
    angle = None
    center = Vector3(0,0,0)
    width, height = (300, 300)
    cubes = []

    def __init__(self):
        self.load()


    def display_text(self, text, x, y, color="white"):
        where = (x, y)
        text_to_show = self.font.render(text, 0, pygame.Color(color))
        self.display.blit(text_to_show, where)

    def display_fps(self):
        text = "FPS: %.02f" % self.clock.get_fps()
        self.display_text(text, 0, 0) # fps
        text = "Player (lv. %d) %s %.0f" % (self.player.level, self.player.orientation(), self.player.sprite_dir) #
        self.display_text(text, 0, 15) # player
        text = "%s (%d, %d)" % (self.map.name, int(self.player.pos.x), int(self.player.pos.y)) #
        self.display_text(text, 0, 30) # map
        text = "angles (%.02f, %.02f, %.02f)" % (self.angle.x, self.angle.y, self.angle.z) #
        self.display_text(text, 0, 45) # map rotation
        text = "direction (%.02f, %.02f, %.02f)" % (self.center.x, self.center.y, self.center.z) #
        self.display_text(text, 0, 60) # cubes offset
        if self.recording:
            text = "◯ Recording"
            self.display_text(text, self.width - 100, 0, color="red") # map rotation

    def display_center(self):
        x = int(self.width / 2)
        y = int(self.height / 2)
        color = (255, 0, 0)
        self.display.set_at((x, y), color)
        self.display.set_at((x, y - 1), color)
        self.display.set_at((x, y - 2), color)
        self.display.set_at((x, y + 1), color)
        self.display.set_at((x, y + 2), color)
        self.display.set_at((x - 1, y), color)
        self.display.set_at((x - 2, y), color)
        self.display.set_at((x + 1, y), color)
        self.display.set_at((x + 2, y), color)

    def set_pixel(self, x, y, color):
        self.display.set_at((x, y), color)

    def record(self):
        self.recording = False if self.recording == True else True

    def movie_frame(self):
        if self.recording == True:
            self.take_screenshot('./movies/')
        pass

    def take_screenshot(self, _path='./screenshots/'):
        if not os.path.isdir(_path):
            os.mkdir(_path)
        filename = "screenshot_%s.bmp" % str(time.time()).replace('.','')
        pygame.image.save(self.display, _path + filename)
        self.log("%s taken." % filename)

    def on_resize(self, width, height):
        self.log("Game resized (%d, %d)." % (width, height))
        self.width = width
        self.height = height
        self.display = pygame.display.set_mode((self.width, self.height), self.flags)
        self.init_terrain()

    def events_keypress(self):
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[pygame.K_p]:
            self.take_screenshot()

        if pressed_keys[pygame.K_F1]:
            self.rotate_camera(0)
            self.init_terrain()
        if pressed_keys[pygame.K_F2]:
            self.rotate_camera(90)
            self.init_terrain()
        if pressed_keys[pygame.K_F3]:
            self.rotate_camera(180)
            self.init_terrain()
        if pressed_keys[pygame.K_F4]:
            self.rotate_camera(270)
            self.init_terrain()
        if pressed_keys[pygame.K_F5]:
            self.rotate_camera(45)
            self.init_terrain()

        if pressed_keys[pygame.K_q]: # map rotate
            self.angle.x = (self.angle.x - 1) % 360
            self.angle.y = (self.angle.y - 1) % 360
            self.angle.z = (self.angle.z + 2) % 360
        if pressed_keys[pygame.K_d]:
            self.angle.x = (self.angle.x + 1) % 360
            self.angle.y = (self.angle.y + 1) % 360
            self.angle.z = (self.angle.z - 2) % 360
            #self.angle.y = (self.angle.y - 10) % 360
        if pressed_keys[pygame.K_z]:
            self.angle.x = (self.angle.x + 10) % 360
        if pressed_keys[pygame.K_s]:
            self.angle.x = (self.angle.x - 10) % 360
        if pressed_keys[pygame.K_a]:
            self.angle.z = (self.angle.z + 10) % 360
        if pressed_keys[pygame.K_e]:
            self.angle.z = (self.angle.z - 10) % 360
        _moved = self.player.move(self.map, pressed_keys)
        if _moved:
            self.init_terrain()
            #self.update_terrain()

    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.VIDEORESIZE:
                _w, _h = event.dict['size']
                self.on_resize(_w, _h)
            elif event.type == pygame.VIDEOEXPOSE:
                self.width, self.height = self.display.get_size()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.running = False
                if event.key == pygame.K_m:
                    self.record()
                    #pygame.mixer.music.play()
                    #pygame.mixer.music.pause()
                    #pygame.mixer.music.unpause()
        self.events_keypress()

    def init_cube(self, x, y):
        center = Vector3(
            float(x) - self.player.pos.x,
            float(y) - self.player.pos.y,
                 0.0 - self.player.pos.z)
        # camera
        center.x += self.center.x
        center.y += self.center.y
        #print(center)
        #center.z += self.center.z
        return (Cube(
            self,
            center,
            Vector3(x, y, 0)
        ))

    def init_terrain(self):
        if not self.map:
            self.log("Map not loaded, can't create cubes.")
            self.quit()
        self.cubes = []
        for y in range(0, self.map.height):
            for x in range(0, self.map.width):
                self.cubes.append(self.init_cube(x, y))

    def rotate_camera(self, angle):
        if angle == 270:
            self.angle = Vector3(0.0, 135.0, 90.0) # terrain angle
            self.center = Vector3(1, 0.5, 0.0)
        elif angle == 180:
            self.angle = Vector3(135.0, 0.0, 180.0) # terrain angle
            self.center = Vector3(.5, 0.0, 0.0)
        elif angle == 90:
            self.angle = Vector3(180.0, 315.0, 90.0) # terrain angle
            self.center = Vector3(0, 0.5, 0.0)
        elif angle == 45:
            self.angle = Vector3(220.0, 320.0, 45.0) # terrain angle
            self.center = Vector3(0, 0.5, 0.0)
        else:
            self.angle = Vector3(225.0, 0.0, 0.0) # terrain angle
            self.center = Vector3(0.5, 1.0, 0.0)

    def load(self):
        self.running = True
        self.recording = False
        self.fps = 100.0
        self.rotate_camera(0)
        pygame.init()
        pygame.font.init() # fonts
        self.font = pygame.font.SysFont("Quicksand", 12)
        #pygame.mixer.init() # audio
        self.clock = pygame.time.Clock() # display
        pygame.display.set_caption("Peepo v1")
        self.flags = pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE
        self.flags = pygame.RESIZABLE
        self.display = pygame.display.set_mode((300,300), self.flags)
        self.display.fill((0,0,0))
        self.player = Player("Player") # player

        # map
        self.map = Map(self.player.map_name if self.player.map_name else 'map00')
        self.init_terrain()

    def loop(self):
        while self.running:
            self.update()
            self.draw()
            self.movie_frame() # if recording

    def update(self):
        _ff = self.clock.get_fps()
        self.player.speed = self.game_speed / _ff if _ff else self.fps
        self.events()
        warp = self.map.check_warps(self.player.pos.x, self.player.pos.y)
        if warp:
            self.map.change(warp['map'])
            self.player.pos.x = warp['position'][0] + .5 # middle of tile
            self.player.pos.y = warp['position'][1] + .5
            self.player.map_name = self.map.name

        self.update_terrain()

    def draw(self):

        self.display.fill((0,0,0)) # clear
        self.draw_scene() # voxel
        self.display_center() # ui
        self.display_fps()

        pygame.display.update()
        self.clock.tick(self.fps)
        pygame.display.flip()
        # print("DRAW")


    def update_terrain(self):
        self.polygons = []
        for cube in self.cubes:
            transformed_vectors = cube.transform_vectors(cube._vectors, self.angle)
            if not self.is_points_on_screen(transformed_vectors):
                continue
            # polygon
            avg_z = cube.calculate_average_z(transformed_vectors)
            for z in avg_z:
                face_index = z[0]
                face = cube._faces[face_index]
                points = cube.create_polygon(face, transformed_vectors)
                if self.is_polygon_on_screen(points):
                    self.polygons.append((points, z[1], cube))
            _vec = [ Vector3(v.x + .5, v.y - .4, v.z + 1.0) for v in [cube._vectors[0]]] # center of top side
            _pts = cube.transform_vectors(_vec, self.angle)[0]
            # decors
            for i in range(0, len(cube.decors)):
                _id = cube.decors[i]['id']
                if _id in self.map.decors: # avoid crash during map change
                    # as little as far
                    _far = 12 * (self.player.pos.y - cube.decors[i]['position'][1])
                    _w = int(self.map.decors[_id]['width'] - _far)
                    _h = int(self.map.decors[_id]['height'] - _far)
                    cube.decors[i]['dest'] = (int(_pts[0] - (_w / 2)), # X
                                              int(_pts[1] - _h + (_w / 15)), # Y
                                              _w, # width
                                              _h # height
                    )
                    cube.decors[i]['sprite_to_draw'] = pygame.transform.scale(self.map.decors_sprite[_id], (_w, _h))
            # items
            for i in range(0, len(cube.items)):
                _id = cube.items[i]['id']
                if _id in self.map.items: # avoid crash during map change
                    cube.items[i]['dest'] = (
                        _pts[0] - (self.map.items[_id]['width'] / 2),
                        _pts[1] - (self.map.items[_id]['height']),
                        self.map.items[_id]['width'],
                        self.map.items[_id]['height'] + 10)
        self.polygons = sorted(self.polygons, key=lambda x: x[1], reverse=True)

    def is_polygon_on_screen(self, points):
        all = [True for pt in points if pt[0] > 0 and pt[0] < self.width and pt[1] > 0 and pt[1] < self.height]
        return (True if True in all else False)

    def is_sprite_on_screen(self, dest):
        if dest[0] + dest[2] < 0 or dest[0] > self.width:
            return (False)
        if dest[1] + dest[3] < 0 or dest[1] > self.height:
            return (False)
        return (True)

    def is_points_on_screen(self, points):
        all = [True for pt in points if pt.x > 0 and pt.x < self.width and pt.y > 0 and pt.y < self.height]
        return (True if True in all else False)

    def draw_scene(self):
        _items_on_floor, _decors = self.draw_terrain()
        _players = [{
            'position': [self.player.pos.x - .5, self.player.pos.y - .5],
            'type': 'PLAYER',
            'dest': (game.width / 2 - 50, game.height / 2 - 85, 77, 77),
            'sprite_to_draw': self.player.sprite
        }]
        for player in _players:
            player['type'] = 'PLAYER'
        _scene = _players + _decors + _items_on_floor
        _scene.sort(key=lambda x: x.get("position")[1])
        for s in _scene:
            if s['type'] == 'PLAYER':
                self.draw_player(s)
            elif s['type'] == 'DECOR':
                self.draw_decoration(s)
            elif s['type'] == 'ITEM':
                self.draw_item_on_floor(s)


    def draw_player(self, player):
        if self.is_sprite_on_screen(player['dest']):
            _tile = int(player['position'][1] + .5) * game.map.width + int(player['position'][0] + .5)
            grd = tuple(self.map.grounds[self.map.tiles[_tile]]["color"])
            col = (grd[0] * .5, grd[1] * .5, grd[2] * .5) # light / 2
            pos = (
                player['dest'][0] + (player['dest'][2] / 2) - 25,
                player['dest'][1] + player['dest'][3],
                70,
                20
            )
            ellipse = pygame.draw.ellipse(self.display, col, pos)
            self.display.blit(player['sprite_to_draw'], player['dest'])

    def draw_decoration(self, decor):
        if decor['id'] in self.map.decors_sprite: # avoid crash during map change
            if self.is_sprite_on_screen(decor['dest']):
                self.display.blit(decor['sprite_to_draw'], decor['dest'])

    def draw_item_on_floor(self, item):
        if self.is_sprite_on_screen(item['dest']):
            if item['id'] in self.map.items_sprite: # avoid crash during map change
                self.display.blit(self.map.items_sprite[item['id']], item['dest'])
                text = "%s" % self.map.items[item['id']]['name']
                self.display_text(text, item['dest'][0], item['dest'][1] + (item['dest'][3] / 2))

    def draw_terrain(self):
        _items_on_floor, _decors = [], []
        # for poly in sorted(self.polygons, key=lambda x: x[1], reverse=True):
        # print("poly = ", len(self.polygons))
        for poly in self.polygons:
            if self.is_polygon_on_screen(poly[0]):
                _cube = poly[2]
                _tex = self.map.grounds[_cube.type]["texture"] if _cube.type in self.map.grounds.keys() else None
                if _tex is not None:
                    # print(_cube.name)
                    # self.draw_quad(self.display, _tex, poly[0])
                    pygame.draw.polygon(self.display, _cube.color, poly[0])
                else:
                    pygame.draw.polygon(self.display, _cube.color, poly[0])
                pygame.draw.polygon(self.display, (0,0,0),     poly[0], 1)
                for decor in _cube.decors:
                    _decors.append(decor)
                for item in _cube.items:
                    _items_on_floor.append(item)
        return (_items_on_floor, _decors)

    def draw_quad(self, surface, img, quad):

        def lerp(p1, p2, f):
            return p1 + f * (p2 - p1)

        def lerp2d(p1, p2, f):
            # return tuple(lerp(p1[i], p2[i], f) for i in range(2))
            return tuple((
                lerp(p1[0], p2[0], f),
                lerp(p1[1], p2[1], f)
            ))

        points = dict()

        img_size = img.get_size()

        for y in range(img_size[1] + 1):
            b = lerp2d(quad[1], quad[2], y / img_size[1])
            c = lerp2d(quad[0], quad[3], y / img_size[1])
            for x in range(img_size[0] + 1):
                a = lerp2d(c, b, x / img_size[0])
                points[(x, y)] = a
                # print((x,y))

                """
                pygame.draw.polygon(
                    surface,
                    img.get_at((x, y)),
                    [
                        (x, y),
                        (x, y + 1),
                        (x + 1, y + 1),
                        (x + 1, y)
                    ]
                )
                """
        # print(len(points.keys()))

        for x in range(img_size[0]):
            for y in range(img_size[1]):
                _pos = points[ (x, y) ]
                _pos = (int(_pos[0]), int(_pos[1]))
                #surface.set_at(
                #    _pos,
                #    img.get_at((x,y))
                #)

                #surface.fill(
                #    img.get_at((x,y)),
                #    (_pos, (1, 1))
                #)
                pygame.draw.polygon(
                    surface,
                    img.get_at((x,y)),
                    [points[(a,b)] for a, b in [
                        (x, y),
                        (x, y + 1),
                        (x + 1, y + 1),
                        (x + 1, y)
                    ]]
                )


    def exit(self):
        pygame.quit()
        sys.exit()

    def run(self):
        self.loop()
        self.map.save()
        self.player.save()
        self.exit()


###
# PLAYER
###

class Player(Entity):

    level = 1
    exp = 0
    bag = []
    gear = []
    pos = Vector3(5.5, 3.5, 0.0)

    def __init__(self, name):
        self.name = name
        _account = self.load(name)
        if _account:
            self.name = _account['name']
            self.level = _account['level']
            self.exp = _account['exp']
            self.bag = _account['bag']
            self.gear = _account['gear']
            self.map_name = _account['current_map']
            self.pos = Vector3(_account['current_pos'][0] + .5,
                               _account['current_pos'][1] + .5,
                               _account['current_pos'][2])
        self.sprite = self.sprites['SE']
        self.sprite_dir = 155.0

    def load(self, name):
        _path = './accounts/'
        if not os.path.isdir(_path):
            os.mkdir(_path)
        if not os.path.isfile(_path + name + '.json'):
            return (False)
        with open(_path + name + '.json', 'r') as fd:
            buf = fd.read()
        account = json.loads(buf)
        self.spritesheet = { # for further spritesheet
            'N': 'imgs/player_N.bmp',
            'NE': 'imgs/player_NE.bmp',
            'E': 'imgs/player_E.bmp',
            'SE': 'imgs/player_SE.bmp',
            'S': 'imgs/player_S.bmp',
            'SO': 'imgs/player_SO.bmp',
            'O': 'imgs/player_O.bmp',
            'NO': 'imgs/player_NO.bmp',
        }
        self.sprites = {}
        for k,v in self.spritesheet.items():
            self.sprites[k] = pygame.image.load(v)
            self.sprites[k] = pygame.transform.scale(self.sprites[k], (96, 96))
        self.log("Account '%s' loaded." % name)
        return (account)

    def save(self, name=None):
        if name != None:
            self.name = name
        _path = './accounts/'
        if not os.path.isdir(_path):
            os.mkdir(_path)
        _account = {
            "name": self.name,
            "level": self.level,
            "exp": self.exp,
            "bag": self.bag,
            "gear": self.gear,
            "current_map": self.map_name,
            "current_pos": (int(self.pos.x), int(self.pos.y), self.pos.z),
        }
        with open(_path + self.name + '.json', 'w') as fd:
            fd.write(json.dumps(_account, indent=4))
        self.log("Account '%s' saved." % self.name)

    def move(self, map, pressed_keys):
        _dir = None
        # direction optimisation
        if   pressed_keys[pygame.K_UP] and pressed_keys[pygame.K_LEFT]: # NO
            self.move_to(map, self.pos.x - self.speed * 0.785, self.pos.y)
            self.move_to(map, self.pos.x, self.pos.y - self.speed * 0.785)
            _dir = 315.0
        elif pressed_keys[pygame.K_UP] and pressed_keys[pygame.K_RIGHT]: # NE
            self.move_to(map, self.pos.x + self.speed * 0.785, self.pos.y)
            self.move_to(map, self.pos.x, self.pos.y - self.speed * 0.785)
            _dir = 45.0
        elif pressed_keys[pygame.K_DOWN] and pressed_keys[pygame.K_LEFT]: # SO
            self.move_to(map, self.pos.x - self.speed * 0.785, self.pos.y)
            self.move_to(map, self.pos.x, self.pos.y + self.speed * 0.785)
            _dir = 225.0
        elif pressed_keys[pygame.K_DOWN] and pressed_keys[pygame.K_RIGHT]: # SE
            self.move_to(map, self.pos.x + self.speed * 0.785, self.pos.y)
            self.move_to(map, self.pos.x, self.pos.y + self.speed * 0.785)
            _dir = 135.0
        elif pressed_keys[pygame.K_LEFT]: # O
            self.move_to(map, self.pos.x - self.speed, self.pos.y)
            _dir = 270.0
        elif pressed_keys[pygame.K_RIGHT]: # E
            self.move_to(map, self.pos.x + self.speed, self.pos.y)
            _dir = 90.0
        elif pressed_keys[pygame.K_UP]: # N
            self.move_to(map, self.pos.x, self.pos.y - self.speed)
            _dir = 0.0
        elif pressed_keys[pygame.K_DOWN]: # S
            self.move_to(map, self.pos.x, self.pos.y + self.speed)
            _dir = 180.0
        if _dir != None:
            self.sprite_dir = _dir
            self.sprite = self.update_sprite()
            return (True)
        return (False)

    def orientation(self):
       _order = ['N','NE','E','SE','S','SO','O','NO']
       return (_order[int((self.sprite_dir + 22.5) / 45.0 % 8)])

    def update_sprite(self):
       return (self.sprites[self.orientation()])

    def update(self):
        pass

    def draw(self, game):
        # shadow on floor
        grd = tuple(game.map.grounds[game.map.tiles[int(self.pos.y) * game.map.width + int(self.pos.x)]]["color"])
        col = (grd[0] * .5, grd[1] * .5, grd[2] * .5) # light / 2
        pos = (game.width / 2 - 35, game.height / 2 - 10, 70, 20)
        ellipse = pygame.draw.ellipse(game.display, col, pos)
        # correct sprite
        #game.display.blit(self.sprite, self.sprite_dest)
        # life bar
        pass

    def move_to(self, map, x, y):
        if map.can_move_to(x, y):
            self.pos.x = x
            self.pos.y = y
            #self.pos.z = z


###
# MAP
###

class Map(Entity):

    def __init__(self, name):
        self.change(name)

    def load(self, name):
        _path = './maps/'
        if not os.path.isdir(_path):
            os.mkdir(_path)
        if not os.path.isfile(_path + name + '.map'):
            self.log("Can't load map '%s'" % name)
            return (False)
        with open(_path + name + '.map', 'r') as fd:
            buf = fd.read()
        _map = json.loads(buf)
        self.name = _map['name']
        self.width = _map['width']
        self.height = _map["height"]
        self.tiles = _map["tiles"]
        self.grounds = _map["grounds"]
        self.grounds_sprites = {}
        for key, ground in self.grounds.items():
            _img = self.grounds[key].get("img", None)
            self.grounds[key]["texture"] = None
            if _img is not None:
                _texture = pygame.image.load(_img)
                _texture = pygame.transform.scale(_texture, (32,32)) # reduce img quality
                self.grounds[key]["texture"] = _texture

        self.tiles_decor = self.look_var(_map, 'tiles_decor', [])
        self.tiles_item = self.look_var(_map, 'tiles_item', [])
        for i in self.tiles_item:
            i['type'] = "ITEM"
        for i in self.tiles_decor:
            i['type'] = "DECOR"
        self.tiles_walk = self.look_var(_map, 'tiles_walk', [])
        self.warps = self.look_var(_map, 'warps', [])
        #self.tiles_item_cpy = list(map['tiles_item'])
        #for item in self.tiles_item:
        #    item["position"][0] += (random.randint(0, 10) / 10.0) - 0.5
        #    item["position"][1] += (random.randint(0, 10) / 10.0) - 0.5
        # decors
        self.decors_sprite = {}
        self.decors = self.look_var(_map, 'decors', {})
        for i, decor in self.decors.items(): # key,value
            self.decors_sprite[i] = pygame.image.load("./" + decor["sprite"])
            self.decors_sprite[i] = pygame.transform.scale(self.decors_sprite[i], (decor['width'], decor['height']))
        # items
        self.items_sprite = {}
        self.items = self.look_var(_map, 'items', {})
        for i, item in self.items.items(): # key,value
            self.items_sprite[i] = pygame.image.load("./" + item["sprite"])
            self.items_sprite[i] = pygame.transform.scale(self.items_sprite[i], (item['width'], item['height']))
        self.log("Map '%s' loaded." % name)
        return (_map)

    def change(self, name):
        self.name = name
        self.load(name)
        self.display()

    def check_warps(self, x, y):
        for _w in self.warps:
            if _w['from'] == [int(x), int(y)]:
                return (_w['to'])
        return (False)


    def display(self):
        buf = "MAP: %s" % self.name + '\n'
        buf += ' ' + "".join([str(int(i % 10)) for i in range(0, self.width)]) + '\n'
        for y in range(0, self.height):
            buf += str(int(y % 10))
            for x in range(0, self.width):
                buf += "%c" % self.tiles[y * self.width + x]
            buf += '\n' if y < self.height - 1 else ''
        self.log(buf)

    def save(self):
        for it in self.tiles_item:
            it.pop('type', None)
            it.pop('dest', None)
        for it in self.tiles_decor:
            it.pop('type', None)
            it.pop('dest', None)
            it.pop('sprite_to_draw', None)
        _grounds_to_save = self.grounds
        for key, gr in _grounds_to_save.items():
            _grounds_to_save[key].pop("texture", None)
        _map = {
            "name": self.name,
            "width": self.width,
            "height": self.height,
            "tiles": self.tiles,
            "grounds": _grounds_to_save,
            "decors": self.decors,
            "items": self.items,
            "tiles_decor": self.tiles_decor,
            "tiles_item": self.tiles_item,
            "tiles_walk": self.tiles_walk,
            "warps": self.warps,
        }
        print(_map)
        print(_map["grounds"])
        tosave = json.dumps(_map, indent=4)
        _path = './maps/'
        if not os.path.isdir(_path):
            os.mkdir(_path)
        with open(_path + _map['name'] + '.map', 'w') as fd:
            fd.write(tosave + '\n')
        self.log("Map '%s' saved." % _map['name'])

    def can_move_to(self, x, y):
        if x < 0 or y < 0 or x > self.width or y > self.height:
            return (False)
        tile = self.tiles[int(y) * self.width + int(x)]
        return (True if tile in self.tiles_walk else False)

#######
# THIS IS THE ENTRY POINT
#######

if __name__ == '__main__':

    print("version:", get_version())
    game = Game()
    game.run()
