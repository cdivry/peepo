# Peepo v1


### Install dependencies

```bash
# create a dedicated environment
python3 -m venv env_peepo

# source it
source env_peepo/bin/activate

# install requirements inside
pip install -r requirements.txt
```

# launch game

with explicit python3 binary :
```bash
python3 peepo.py
```

or directly from shebang :

```bash
./peepo.py
```
